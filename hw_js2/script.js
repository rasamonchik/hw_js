// Перше завдання
let min = Math.floor(Math.random() * 60)
if (min >= 0 && min <= 14) {
    console.log('Перша четверть')
}
if (min >= 15 && min <= 29) {
    console.log('Друга четверть')
}
if (min >= 30 && min <= 44) {
    console.log('Третя четверть')
} else if (min >= 45 && min <= 59) {
    console.log('Четверта четверть')
}

// Друге завдання
let lang = 'en'
let arr
//Array.from('Понеділок, Вівторок, Середа, Четвер, Пятниця, Субота, Неділя')
if (lang === 'ua') {
    arr = ["Понеділок", "Вівторок", "Середа", "Четвер", "Пятниця", "Субота", "Неділя"]
}
if (lang === 'en') {
    arr = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
}
console.log(arr)

// Третє завдання через Math
let str = [Math.floor(Math.random() * 10000000)]
if (str[0] + str[1] + str[2] === str[5] + str[6] + str[7]) {
    console.log('Згенероване число: '`${str}`, 'Щасливий!')
} else {
    console.log(`${str}`, 'Вам не пощастило')
}

// Третє завдання через масив
let set = ['1', '1', '1', '4', '5', '6', '7', '1', '1', '1']
if (set[0] + set[1] + set[2] === set[7] + set[8] + set[9]) {
    console.log('Щасливий!')
} else {
    console.log('Вам не пощастило')
}
// Третє завдання через рядок
let src = '123456789'
let num = Number(src)
let FirstSum = Number(src[0]) + Number(src[1]) + Number(src[2])
let LastSum = Number(str[6]) + Number(str[7]) + Number(str[8])
if (FirstSum === LastSum) {
    console.log('Щасливий!')
} else {
    console.log('Вам не пощастило')
}